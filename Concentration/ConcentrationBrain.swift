//
//  ConcentrationBrain.swift
//  Concentration
//
//  Created by Ionut Flocea on 18.02.2022.
//

import Foundation
import UIKit

struct ConcentrationBrain {
    
    var images : [UIImage] = [#imageLiteral(resourceName: "SoccerBall"), #imageLiteral(resourceName: "BasketBall"), #imageLiteral(resourceName: "Skateboard"), #imageLiteral(resourceName: "GolfStick"), #imageLiteral(resourceName: "Bicycle"), #imageLiteral(resourceName: "Tennis"), #imageLiteral(resourceName: "SoccerBall"), #imageLiteral(resourceName: "BasketBall"), #imageLiteral(resourceName: "Skateboard"), #imageLiteral(resourceName: "GolfStick"), #imageLiteral(resourceName: "Bicycle"), #imageLiteral(resourceName: "Tennis") ]
    
    var score : Int
    
    mutating func shuffleImages() {
        images.shuffle()
    }
    
    init() {
        self.score = 0
        shuffleImages()
    }
    
    
}
