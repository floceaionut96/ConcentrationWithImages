//
//  ViewController.swift
//  Concentration
//
//  Created by Ionut Flocea on 18.02.2022.
//

import UIKit

class ViewController: UIViewController {

   
    @IBOutlet weak var btt1: UIButton!
    @IBOutlet weak var btt2: UIButton!
    @IBOutlet weak var btt3: UIButton!
    @IBOutlet weak var btt4: UIButton!
    @IBOutlet weak var btt5: UIButton!
    @IBOutlet weak var btt6: UIButton!
    @IBOutlet weak var btt7: UIButton!
    @IBOutlet weak var btt8: UIButton!
    @IBOutlet weak var btt9: UIButton!
    @IBOutlet weak var btt10: UIButton!
    @IBOutlet weak var btt11: UIButton!
    @IBOutlet weak var btt12: UIButton!
    @IBOutlet weak var scoreLabel: UILabel!
    
    lazy var buttons : [UIButton] = [btt1,btt2,btt3,btt4,btt5,btt6,btt7,btt8,btt9,btt10,btt11,btt12]
    
    lazy var pairOfCards : [UIButton] = []
    
    var concBrain = ConcentrationBrain()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        scoreLabel.text = "SCORE :  \(concBrain.score)"
        
        setBackOnCards()
    }
    @IBAction func cardPressed(_ sender: UIButton) {
        
        sender.setBackgroundImage(concBrain.images[sender.tag-1], for: UIControl.State.normal)

        
        pairOfCards.append(sender)
        
        if pairOfCards.count == 2 {
            
            
            if checkIfRightPair(pairs: pairOfCards){
                Timer.scheduledTimer(timeInterval: 0.5, target: self, selector: #selector(setOkImage), userInfo: nil, repeats: false)
                concBrain.score += 3
            
            } else {
                let backImage = #imageLiteral(resourceName: "Back")
                pairOfCards[0].setBackgroundImage(backImage, for: UIControl.State.normal)
                pairOfCards[1].setBackgroundImage(backImage, for: UIControl.State.normal)
                pairOfCards.removeAll()
                concBrain.score -= 1
            }
        }
        updateScore()
    }
    
    func setBackOnCards(){
        let backImage = #imageLiteral(resourceName: "Back")
        for i in 0...(buttons.count-1){
            buttons[i].setBackgroundImage(backImage, for: UIControl.State.normal)
        }
    }
    
    func checkIfRightPair(pairs : [UIButton]) -> Bool{
        let data1 = pairs[0].currentBackgroundImage?.pngData()
        let data2 = pairs[1].currentBackgroundImage?.pngData()
        return data1 == data2
    }
    
    @objc func setOkImage(){
        let oK = #imageLiteral(resourceName: "OK")
        pairOfCards[0].setBackgroundImage(oK, for: UIControl.State.normal)
        pairOfCards[1].setBackgroundImage(oK, for: UIControl.State.normal)
        pairOfCards[0].isEnabled = false
        pairOfCards[1].isEnabled = false
        pairOfCards.removeAll()
    }

    func updateScore(){
        scoreLabel.text = "SCORE :  \(concBrain.score)"
    }
    
}

